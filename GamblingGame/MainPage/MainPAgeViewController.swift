//
//  MainPAgeViewController.swift
//  GamblingGame
//
//  Created by A. Buğrahan Taşdan on 14.12.2017.
//  Copyright © 2017 A. Buğrahan Taşdan. All rights reserved.
//

import UIKit
import DragDropiOS
import AVFoundation
import Sica


class MainPAgeViewController: UIViewController,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, DragDropCollectionViewDelegate{
    
    @IBOutlet weak var diceSumLabel: UILabel!
    @IBOutlet weak var thirdDice: UIImageView!
    @IBOutlet weak var diceLabel: UILabel!
    @IBOutlet weak var rightDice: UIImageView!
    @IBOutlet weak var leftDice: UIImageView!
    var models:[Model] = [Model]()
    var tableModels:[Model] = [Model]()
    
    var player: AVAudioPlayer?

    
    
    var dragDropManager:DragDropManager!
    
    @IBOutlet weak var topDragDropCollectionView: DragDropCollectionView!
    @IBOutlet weak var totalAccount: UILabel!
    @IBOutlet weak var dragDropCollectionView:DragDropCollectionView!
    
    @IBOutlet weak var dragDropView:DragDropView!
    @IBOutlet weak var totalMoneyOutlet: UILabel!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        let mainPageCollectionViewCellNib=UINib(nibName: "CollectionViewCell", bundle: Bundle.main)
        dragDropCollectionView.register(mainPageCollectionViewCellNib, forCellWithReuseIdentifier: "CollectionViewCell")
        topDragDropCollectionView.register(mainPageCollectionViewCellNib, forCellWithReuseIdentifier: "CollectionViewCell")
        for i in 0 ..< 20 {
            let model = Model()
            
            models.append(model)
            // Do any additional setup after loading the view.
        }
            let model = Model()
            let fruit = Fruit(id: 1, name: "0",imageName:"tokenNew")
            model.fruit = fruit
            models.append(model)
            dragDropView.model = model
        rightDice.isHidden = true
        leftDice.isHidden = true
        thirdDice.isHidden = true
        diceSumLabel.isHidden = true
        
        dragDropCollectionView.backgroundColor = UIColor(red:0.886,  green:0.341,  blue:0.255, alpha:1)
        dragDropCollectionView.bounces = false
        dragDropCollectionView.dragDropDelegate = self
        topDragDropCollectionView.backgroundColor = UIColor(red:0.886,  green:0.341,  blue:0.255, alpha:1)
        topDragDropCollectionView.bounces = false
        topDragDropCollectionView.dragDropDelegate = self
        dragDropManager = DragDropManager(canvas: self.view, views: [dragDropView,dragDropCollectionView,topDragDropCollectionView])
        
       
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == dragDropCollectionView){
            return CGSize(width: collectionView.frame.size.width/8, height: collectionView.frame.size.height/2-10)
        }else{
            return CGSize(width: collectionView.frame.size.width/7, height: collectionView.frame.size.height-5)
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == dragDropCollectionView){
            return 14
        }else {
            return 6
        }
    }
   
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == topDragDropCollectionView){
            let model = models[indexPath.item+14]
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
            let temp = (indexPath.item + 1) * 3
            cell.locationLabel.text=String(temp)
            cell.updateData(model)
            return cell

        }else{
            let model = models[indexPath.item]
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
            let temp = indexPath.item + 4
            cell.locationLabel.text=String(temp)
            cell.updateData(model)
            return cell

        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, touchBeginAtIndexPath indexPath: IndexPath) {
        
        if(collectionView == dragDropCollectionView){
            clearCellsDrogStatusOfCollectionView()
        }else{
            clearCellsDrogStatusForTop()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, representationImageAtIndexPath indexPath: IndexPath) -> UIImage? {
        
        
            if let cell = collectionView.cellForItem(at: indexPath) {
                UIGraphicsBeginImageContextWithOptions(cell.bounds.size, false, 0)
                cell.layer.render(in: UIGraphicsGetCurrentContext()!)
                
                let img = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                return img
            }
        
        return nil
        
    }
    
    func collectionView(_ collectionView: UICollectionView, canDragAtIndexPath indexPath: IndexPath) -> Bool {
        if(collectionView == dragDropCollectionView){
            return models[indexPath.item].fruit != nil
        }else{
            return models[indexPath.item+14].fruit != nil
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, dragInfoForIndexPath indexPath: IndexPath) -> AnyObject {
        if(collectionView == dragDropCollectionView){
            let dragInfo = Model()
           
            dragInfo.fruit = models[indexPath.item].fruit
            
            return dragInfo
        }else{
            let dragInfo = Model()
            dragInfo.fruit = models[indexPath.item + 14].fruit
            return dragInfo
        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, canDropWithDragInfo dataItem: AnyObject, AtIndexPath indexPath: IndexPath) -> Bool {
      
        return true
    }
    
    
    func collectionView(_ collectionView: UICollectionView, dropOutsideWithDragInfo info: AnyObject) {
        if(collectionView == dragDropCollectionView){
            clearCellsDrogStatusOfCollectionView()
        }else{
            clearCellsDrogStatusForTop()
        }
        dragDropCollectionView.reloadData()
        topDragDropCollectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, dragCompleteWithDragInfo dragInfo:AnyObject, atDragIndexPath dragIndexPath: IndexPath,withDropInfo dropInfo:AnyObject?) -> Void{
        if(collectionView == dragDropCollectionView){
              models[dragIndexPath.item].fruit = nil
        }else{
              models[dragIndexPath.item + 14].fruit = nil
        }
        
            
    }
    
    func collectionView(_ collectionView: UICollectionView, dropCompleteWithDragInfo dragInfo:AnyObject, atDragIndexPath dragIndexPath: IndexPath?,withDropInfo dropInfo:AnyObject?,atDropIndexPath dropIndexPath:IndexPath) -> Void{
        playChipSound()
        if(collectionView == dragDropCollectionView){
            let tempModel = models[dropIndexPath.item].fruit
            if tempModel == nil{
                models[dropIndexPath.item].fruit = (dragInfo as! Model).fruit
            }else{
                let temp = Int((tempModel?.name)!)! + Int(((dragInfo as! Model).fruit?.name)!)!
                (dragInfo as! Model).fruit?.name = String(temp)
                models[dropIndexPath.item].fruit = (dragInfo as! Model).fruit
                
            }
        }else{
            let tempModel = models[dropIndexPath.item + 14].fruit
            if tempModel == nil{
                models[dropIndexPath.item+14].fruit = (dragInfo as! Model).fruit
            }else{
                let temp = Int((tempModel?.name)!)! + Int(((dragInfo as! Model).fruit?.name)!)!
                (dragInfo as! Model).fruit?.name = String(temp)
                models[dropIndexPath.item+14].fruit = (dragInfo as! Model).fruit
                
            }
        }
        
    }
   
    func collectionViewStopDropping(_ collectionView: UICollectionView) {
        
        if(collectionView == dragDropCollectionView){
            clearCellsDrogStatusOfCollectionView()
        }else{
            clearCellsDrogStatusForTop()
        }
        
        dragDropCollectionView.reloadData()
        topDragDropCollectionView.reloadData()
    }
    
    func collectionViewStopDragging(_ collectionView: UICollectionView) {
        if(collectionView == dragDropCollectionView){
            clearCellsDrogStatusOfCollectionView()
        }else{
            clearCellsDrogStatusForTop()
        }
        dragDropCollectionView.reloadData()
        topDragDropCollectionView.reloadData()
    }
    
    
    func clearCellsDrogStatusOfCollectionView(){
        
        for cell in dragDropCollectionView.visibleCells{
            
            if (cell as! CollectionViewCell).hasContent() {
                (cell as! CollectionViewCell).selectedStatus()
                continue
            }
            
            (cell as! CollectionViewCell).nomoralStatus()
            
            
        }
       
        
    }
    func clearCellsDrogStatusForTop(){
        for cell in topDragDropCollectionView.visibleCells{
            
            if (cell as! CollectionViewCell).hasContent() {
                (cell as! CollectionViewCell).selectedStatus()
                continue
            }
            
            (cell as! CollectionViewCell).nomoralStatus()
            
            
        }
    }
    @IBAction func minusAction(_ sender: UIButton) {
        if(dragDropView.model != nil){
            let bet:Int
            if let temp = Int((dragDropView.model?.fruit?.name)!){
                if(temp > 9){
                    bet = temp - 10
                    dragDropView.model?.fruit?.name = String(describing: bet)
                    if let total = Int(totalMoneyOutlet.text!){
                        let tempTotal:Int
                        tempTotal = total + 10
                        dragDropView.model?.fruit?.name = String(bet)
                        totalMoneyOutlet.text = String(tempTotal)
                        dragDropView.updateLabel()
                        if(bet==0){
                            dragDropView.model = nil
                        }
                    }
                }
            
            }
        }
    }
    @IBAction func plusAction(_ sender: UIButton) {
        if(dragDropView.model != nil){
            let bet:Int
            if let temp = Int((dragDropView.model?.fruit?.name)!){
                
                bet = temp + 10
                dragDropView.model?.fruit?.name = String(bet)
                if let total = Int(totalMoneyOutlet.text!){
                    if(total>0){
                        let tempTotal:Int
                        tempTotal = total - 10
                   
                        dragDropView.model?.fruit?.name = String(bet)
                        totalMoneyOutlet.text = String(tempTotal)
                        dragDropView.updateLabel()
                    }
                }
            }
        }else{
            let model = Model()
            let fruit = Fruit(id: 2, name: "0",imageName:"tokenNew")
            model.fruit = fruit
            models.append(model)
            dragDropView.model = model
            let bet:Int
            if let temp = Int((dragDropView.model?.fruit?.name)!){
                
                bet = temp + 10
                dragDropView.model?.fruit?.name = String(bet)
                if let total = Int(totalMoneyOutlet.text!){
                    let tempTotal:Int
                    tempTotal = total - 10
                    
                    dragDropView.model?.fruit?.name = String(bet)
                    totalMoneyOutlet.text = String(tempTotal)
                    dragDropView.updateLabel()
                    
                }
            }
            
        }
    }
    
    @IBAction func rollDice(_ sender: UIButton) {
        
        var imagesArray1 = [UIImage]()
        var imagesArray2 = [UIImage]()
        var imagesArray3 = [UIImage]()
        
        let numberOne = arc4random_uniform(6) + 1
        
        let numberTwo = arc4random_uniform(6) + 1
        
        let numberThird = arc4random_uniform(6) + 1
        
        leftDice.isHidden = false
        rightDice.isHidden = false
        thirdDice.isHidden = false
        diceSumLabel.isHidden = false
        diceSumLabel.alpha = 1.0
        leftDice.alpha = 1.0
        rightDice.alpha = 1.0
        thirdDice.alpha = 1.0
        
        playDiceSound()
      
        for i in 0..<34 {
            let numberOne = arc4random_uniform(6) + 1
            
            let numberTwo = arc4random_uniform(6) + 1
            
            let numberThird = arc4random_uniform(6) + 1
            
            imagesArray1.append(UIImage(named: "Dice\(numberOne)")!)
            imagesArray2.append(UIImage(named: "Dice\(numberTwo)")!)
            imagesArray3.append(UIImage(named: "Dice\(numberThird)")!)
        }
        leftDice.animationImages = imagesArray1
        rightDice.animationImages = imagesArray2
        thirdDice.animationImages = imagesArray3
        
        leftDice.animationDuration = 5
        rightDice.animationDuration = 5
        thirdDice.animationDuration = 5
        
        leftDice.animationRepeatCount = 1
        rightDice.animationRepeatCount = 1
        thirdDice.animationRepeatCount = 1
        
        
        leftDice.startAnimating()
        rightDice.startAnimating()
        thirdDice.startAnimating()
        
        leftDice.image = (UIImage(named: "Dice\(numberOne)")!)
        rightDice.image = (UIImage(named: "Dice\(numberTwo)")!)
        thirdDice.image = (UIImage(named: "Dice\(numberThird)")!)
        
        
        
//        leftDice.image = UIImage(named: "Dice\(numberOne)")
//        rightDice.image = UIImage(named: "Dice\(numberTwo)")
//        thirdDice.image = UIImage(named: "Dice\(numberThird)")
        
        
//        let animator = Animator(view: leftDice)
//        animator
//            .addBasicAnimation(keyPath: .positionX, from: 50, to: 150, duration: 2, timingFunction: .easeOutExpo)
//            .addSpringAnimation(keyPath: .boundsSize, from: leftDice.frame.size, to: CGSize(width: 240, height: 240), damping: 12, mass: 1, stiffness: 240, initialVelocity: 0, duration: 1)
//            .run(type: .sequence)
        
        let animatorLeft = Animator(view: leftDice)
        animatorLeft
            .addBasicAnimation(keyPath: .positionY, from: 50, to: view.center.y - leftDice.frame.size.height/2, duration: 0.5, timingFunction: .easeOut)
//            .addSpringAnimation(keyPath: .boundsSize, from: leftDice.frame.size, to: CGSize(width: 180, height: 180), damping: 12, mass: 1, stiffness: 240, initialVelocity: 0, duration: 1)
            .run(type: .sequence)
        
        let animatorMiddle = Animator(view: rightDice)
        animatorMiddle
            .addBasicAnimation(keyPath: .positionY, from: 50, to: view.center.y - leftDice.frame.size.height/2, duration: 0.5, timingFunction: .easeOut)
//            .addSpringAnimation(keyPath: .boundsSize, from: rightDice.frame.size, to: CGSize(width: 180, height: 180), damping: 12, mass: 1, stiffness: 240, initialVelocity: 0, duration: 1)
            .run(type: .sequence)
        
        let animatorRight = Animator(view: thirdDice)
        animatorRight
            .addBasicAnimation(keyPath: .positionY, from: 50, to: view.center.y - leftDice.frame.size.height/2, duration: 0.5, timingFunction: .easeOut)
//            .addSpringAnimation(keyPath: .boundsSize, from: thirdDice.frame.size, to: CGSize(width: 180, height: 180), damping: 12, mass: 1, stiffness: 240, initialVelocity: 0, duration: 1)
            .run(type: .sequence)
        
        
        // Then fades it away after 2 seconds (the cross-fade animation will take 0.5s)
        UIView.animate(withDuration: 2, delay:6, options:UIViewAnimationOptions.transitionFlipFromTop, animations: {
            self.rightDice.alpha = 0
            self.leftDice.alpha = 0
            self.thirdDice.alpha = 0
            self.diceSumLabel.alpha = 0
        }, completion: { finished in
            self.rightDice.isHidden = true
            self.leftDice.isHidden = true
            self.thirdDice.isHidden = true
            self.diceSumLabel.isHidden = true
        })
        let winNumber = numberOne + numberTwo + numberThird
        var count = 4
        diceSumLabel.text = String(winNumber)
        for i in 0 ..< 14 {
            let overInfo = models[i]
            if(overInfo.fruit == nil){
               
            }else{
                if(count==winNumber){
                    if let total = Int(totalMoneyOutlet.text!){
                        let tempTotal:Int
                        tempTotal = total + 2*Int((overInfo.fruit?.name)!)!
                        totalMoneyOutlet.text = String(tempTotal)
                        
                    
                    }
                }else{
                    
                }
            }
          
            count = count + 1
            
        }
        count = 3
        for i in 14 ..< 20 {
            let overInfo = models[i]
            if(overInfo.fruit == nil){
                
            }else{
                if(numberOne == numberTwo && numberOne == numberThird && numberOne == count){
                    if let total = Int(totalMoneyOutlet.text!){
                        let tempTotal:Int
                        tempTotal = total + 5*Int((overInfo.fruit?.name)!)!
                        totalMoneyOutlet.text = String(tempTotal)
                        
                        
                    }
                }else{
                    
                }
            }
            
            count = count + 3
            
        }
        models = [Model]()
        for i in 0 ..< 20 {
            let model = Model()
            models.append(model)
        }
        dragDropCollectionView.reloadData()
        topDragDropCollectionView.reloadData()
    }
    
    func playDiceSound() {
        guard let url = Bundle.main.url(forResource: "RollDice", withExtension: "wav") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func playChipSound() {
        guard let url = Bundle.main.url(forResource: "chipsHandle6", withExtension: "wav") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
            
}
