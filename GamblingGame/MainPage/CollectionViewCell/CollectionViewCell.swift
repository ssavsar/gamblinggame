//
//  CollectionViewCell.swift
//  GamblingGame
//
//  Created by A. Buğrahan Taşdan on 14.12.2017.
//  Copyright © 2017 A. Buğrahan Taşdan. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var labelBet: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    var model:Model!
    
    override func awakeFromNib() {
        backgroundColor = UIColor(red:0.886,  green:0.341,  blue:0.255, alpha:1)
//        layer.borderColor = UIColor.white.cgColor
//        layer.borderWidth = 1
        
    }
    
    
    func updateData(_ model:Model){
        self.model = model
        if model.fruit != nil {
            selectedStatus()
            imageView.image = UIImage(named: model.fruit!.imageName!)
            labelBet.text = model.fruit?.name
        }else{
            nomoralStatus()
            imageView.image = nil
        }
    }
    
    func hasContent() -> Bool {
        return model.fruit != nil
    }
    
    func moveOverStatus(){
        backgroundColor = UIColor.orange.withAlphaComponent(0.5)
    }
    
    func nomoralStatus(){
        backgroundColor = UIColor(red:0.886,  green:0.341,  blue:0.255, alpha:1)
        labelBet.text = ""
    }
    
    func selectedStatus(){
//        backgroundColor = UIColor.blue.withAlphaComponent(0.5)
    }
    
    
    

}
