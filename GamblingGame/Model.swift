//
//  Model.swift
//  GamblingGame
//
//  Created by A. Buğrahan Taşdan on 14.12.2017.
//  Copyright © 2017 A. Buğrahan Taşdan. All rights reserved.
//


import Foundation
class Model {
    var collectionIndex:Int? 
    var fruit:Fruit?
}


class Fruit {
    var id:Int!
    var name:String?
    var imageName:String?
    init(id:Int,name:String,imageName:String) {
        self.id = id
        self.name = name
        self.imageName = imageName
    }
}
